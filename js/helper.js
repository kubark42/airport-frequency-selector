.pragma library

.import "qrc:/js/physical_constants.js" as Phys_cnsts

//! Express seconds of time in dd:hh:mm format, unless sec_min_border is
//! set, which then outputs in mm:ss format
function formatTime(sec, sec_min_border)
{
    var value = Math.floor(sec)

    // Extract seconds
    var seconds = value % 60

    // Convert to round minutes
    var minutes = Math.floor(value / 60) % 60

    // Convert to round hours
    var hours = Math.floor(value / 3600) % 24

    // Convert to round days
    var days = Math.floor(value / (3600*24))

    var hhmmss_str
    if (sec < sec_min_border) {
        // Create string and pad with 0s
        hhmmss_str = minutes + "m" +
                ('00'+seconds).slice(-2) + "s"
    } else {
        // Create string and pad with 0s
        if (days > 0) {
            // Only show days and hours
            hhmmss_str = days + "d" +
                    ('00'+hours).slice(-2) + "h"
        } else if (hours > 0) {
            // Only show hours and minutes
            hhmmss_str = hours + "h" +
                    ('00'+minutes).slice(-2) + "m"
        } else {
            hhmmss_str = minutes + "m"
        }
    }

    return hhmmss_str
}


/** zeroPad (num, numDigits)
 *  pads a number, `num`, with leading zeros, up to the specified number of digits, `numDigits`
 */
function zeroPad (num, numDigits) {
    // Handle the 0 value correctly
    if (num === 0) {
        var zeroString = []
        for (var i=0; i<numDigits; i++) {
            zeroString += '0'
        }

        return '+' + zeroString;
    }

    var an = Math.abs(num);

    var digitCount = 1 + Math.floor (Math.log (an) / Math.LN10);
    if (digitCount >= numDigits) {
        return num;
    }
    var zeroString = Math.pow (10, numDigits - digitCount).toString ().substr (1);
    if (num < 0) {
        return '-' + zeroString + an;
    } else {
        return '+' + zeroString + an;
    }
}


/** formatDistance(distance_m)
 *  Takes a distance in meters and formats it into a round(ish) string
 */
function formatDistance(distance_m)
{
    var distanceStr
    // Check if distance will be expressed in [m] or [km]
    if (distance_m > 1000 ) {
        // If the distance is greater than 100km, don't use decimal places
        if (distance_m > 100000) {
            distanceStr = Math.round(distance_m / 1000)
        } else{
            distanceStr = Math.round(distance_m / 100) / 10
        }

        // Add [km] units to string
        distanceStr = distanceStr + " km"
    } else{
        // Round and add [m] units to string
        distanceStr = Math.round(distance_m) + " m"
    }

    return distanceStr
}


function textualAisNavigationStatus(navStatus) {
    switch (navStatus) {
    case 0:
        return "Under way using engine"
        break;
    case 1:
        return "At anchor"
        break;
    case 2:
        return "Not under command"
        break;
    case 3:
        return "Restriced maneuverability"
        break;
    case 4:
        return "Constrained by its draft"
        break;
    case 5:
        return "Moored"
        break;
    case 6:
        return "Aground"
        break;
    case 7:
        return "Fishing"
        break;
    case 8:
        return "Sailing"
        break;
    case 9:
        return "Reserved for high-speed craft"
        break;
    case 10:
        return "Reserved for ground-effect craft"
        break;
    case 11:
        return "Tugging astern"
        break;
    case 12:
        return "Pushing ahead or towing alongside"
        break;
    case 13:
        return "Reserved"
        break;
    case 14:
        return "AIS-SART (active), MOB-AIS, EPIRB-AIS"
        break;
    case 15:
        return "Undefined/default/test SART/MOD/EPIRB"
        break;
    }

}


function base(val) {
    if (val <= -0.75) return 0;
    else if (val <= -0.25) return interpolate(val, -0.75, 0.0, -0.25, 1.0);
    else if (val <= 0.25) return 1.0;
    else if (val <= 0.75) return interpolate(val, 0.25, 1.0, 0.75, 0.0);
    else return 0.0;
}

var max_color_range = 1.0  // This is either 1.0 or 255, depending on the colorspace scaling

function red(gray) {
    return base(gray - 0.5)*max_color_range
}
function green(gray) {
    return base(gray)*max_color_range
}
function blue(gray) {
    return base(gray + 0.5)*max_color_range
}

// Returns a value of `x` linearly interpolated between x0 and x1
function interpolate(val, x0, y0, x1, y1) {
    return (val-x0)*(y1-y0)/(x1-x0) + y0;
}



function getN(latitude_R)
{
    var wgs84_a =                       6378.137 // semi-major axis of the ellipsoid, in [km]
    var wgs84_b =                   6356.7523142 // semi-minor axis of the ellipsoid, in [km]
    var a    = wgs84_a  // wgs84.RADIUS;
    var b    = wgs84_b  // wgs84.POLAR_RADIUS;
    var asqr = a*a;
    var bsqr = b*b;

    var e = Math.sqrt((asqr-bsqr)/asqr);


    var sinlatitude = Math.sin(latitude_R);
    var denom = Math.sqrt(1-e*e*sinlatitude*sinlatitude);
    var N = a / denom;
    return N;
}

function lla2ecef(latitude_D, longitude_D, altitude)
{
    var deg2rad = Math.PI / 180
    var latitude_R = latitude_D * deg2rad
    var longitude_R = longitude_D * deg2rad
    /*
     * Some constants we'll want to have on hand
     */
    var wgs84_radius_earth_km =         6371.008 // Earth's radius, in [km]
    var wgs84_a =                       6378.137 // semi-major axis of the ellipsoid, in [km]
    var wgs84_b =                   6356.7523142 // semi-minor axis of the ellipsoid, in [km]
    var wgs84_flattening =   3.35281066474748e-3 // flattening, i.e. (1 / 298.257223563)
    var wgs84_eps =           8.1819190842622e-2 // first eccentricity, i.e. sqrtf(1-WGS84_B^2/WGS84_A^2)
    var wgs84_eps2 =              6.694379990e-3 // first eccentricity squared, i.e. WGS84_EPS^2

    var a    = wgs84_a  // wgs84.RADIUS;
    var f    = wgs84_flattening  // wgs84.FLATTENING;
    var b    = wgs84_b  // wgs84.POLAR_RADIUS;
    var asqr = a*a;
    var bsqr = b*b;

    var e = Math.sqrt((asqr-bsqr)/asqr);
    var eprime = Math.sqrt((asqr-bsqr)/bsqr);

    //Auxiliary values first
    var N = getN(latitude_R);
    var ratio = (bsqr / asqr);

    //Now calculate the Cartesian coordinates
    var X = (N + altitude) * Math.cos(latitude_R) * Math.cos(longitude_R) * 1000;
    var Y = (N + altitude) * Math.cos(latitude_R) * Math.sin(longitude_R) * 1000;

    //Sine of latitude looks right here
    var Z = (ratio * N + altitude) * Math.sin(latitude_R) * 1000;

    return [X, Y, Z];
}

function ecef2lla(X, Y, Z)
{
    /*
     * Some constants we'll want to have on hand
     */
    var wgs84 = require('wgs84');
    var a    = wgs84.RADIUS;
    var f    = wgs84.FLATTENING;
    var b    = wgs84.POLAR_RADIUS;
    var asqr = a*a;
    var bsqr = b*b;

    var e = Math.sqrt((asqr-bsqr)/asqr);
    var eprime = Math.sqrt((asqr-bsqr)/bsqr);

    //Auxiliary values first
    var p = Math.sqrt(X*X + Y*Y);
    var theta_R = Math.atan((Z*a)/(p*b));

    var sT = Math.sin(theta_R);
    var cT = Math.cos(theta_R);

    var num = Z + eprime * eprime * b * sT * sT * sT;
    var denom = p - e * e * a * cT * cT * cT;

    //Now calculate LLA
    var latitude  = Math.atan(num/denom);
    var longitude = Math.atan(Y/X);
    var N = getN(latitude);
    var altitude  = (p / Math.cos(latitude)) - N;

    if (X < 0 && Y < 0) {
        longitude = longitude - Math.PI;
    }

    if (X < 0 && Y > 0) {
        longitude = longitude + Math.PI;
    }

    return [latitude, longitude, altitude];
}

function sanitizePositionData(positionSource, currentPositionCoordinate, previousPositionCoordinate)
{
    // Position objects are write-only, so we have to make a special variable which can be overwritten
    var tmpPositionCoordinate = previousPositionCoordinate
    previousPositionCoordinate = currentPositionCoordinate
    currentPositionCoordinate = positionSource.position.coordinate

    // Sanitize input data
    if (!isNaN(positionSource.position.direction)) {
        currentTrackAngle_deg = positionSource.position.direction
    }
    if (!isNaN(positionSource.position.horizontalAccuracy)) {
        horizonalPositionalAccuracy = positionSource.position.horizontalAccuracy
    }
    if (!isNaN(positionSource.position.timestamp)) {
        previousPositionTimestamp = currentPositionTimestamp
        currentPositionTimestamp = positionSource.position.timestamp
    }
    if (!isNaN(positionSource.position.speed)) {
        currentSpeedOverGround_mps = positionSource.position.speed
    } else {
        console.log("[Position]: speed, " + positionSource.position.speed +", not valid: " + positionSource.position.speedValid)
    }

    if (isNaN(currentPositionCoordinate.altitude)) {
        currentPositionCoordinate.altitude = tmpPositionCoordinate.altitude
    }

    if (isNaN(currentPositionCoordinate.latitude) ||
            isNaN(currentPositionCoordinate.longitude)) {
        currentPositionCoordinate.latitude = tmpPositionCoordinate.latitude
        currentPositionCoordinate.longitude = tmpPositionCoordinate.longitude
    }

    return [currentPositionCoordinate, previousPositionCoordinate];
}

function velocityMadeGoodToMarker(speedOverGround_mps, trackAngle_deg, vehicle_coordinate, marker_coordinate)
{
    // Calculate the bearing to the marker
    var markerBearing_deg = vehicle_coordinate.azimuthTo(marker_coordinate)

    // Calculate the angle between track and marker bearing
    var theta_rad = Math.PI/180 * (markerBearing_deg - trackAngle_deg)

    // Calculate velocity made good toward the marker
    var vmg = Math.cos(theta_rad) * speedOverGround_mps

    return vmg
}

function calculateLinerization(latitude_R) {
    let cLat = Math.cos(latitude_R)
    let sLat = Math.sin(latitude_R)

    let a2 = Math.pow(Phys_cnsts.wgs84_a * 1000, 2);  // Convert into meters
    let b2 = Math.pow(Phys_cnsts.wgs84_b * 1000, 2);  // Convert into meters
    let Rf = Math.sqrt(
            (a2*a2*cLat*cLat + b2*b2*cLat*sLat) /
            (a2*cLat*cLat + b2*cLat*sLat)
            )

    let linearized_conversion_factor_dN = Rf;
    let linearized_conversion_factor_dE = cLat*Rf

    return {dN: linearized_conversion_factor_dN, dE: linearized_conversion_factor_dE};
}



// Calculate the bearing (in [rad]) from the first point to the second point
// C.f. https://www.movable-type.co.uk/scripts/latlong.html
function bearingLatLon(latitude1_R, longitude1_R, latitude2_R, longitude2_R) {
    let φ1 = latitude1_R;
    let λ1 = longitude1_R;
    let φ2 = latitude2_R;
    let λ2 = longitude2_R;

    let y = Math.sin(λ2-λ1) * Math.cos(φ2);
    let x = Math.cos(φ1)*Math.sin(φ2) -
            Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1);
    let brng_R = Math.atan2(y, x);

    return brng_R
}


// Using the Haversine formula, calculate the distance (in [m]) between the two locations
// C.f. https://www.movable-type.co.uk/scripts/latlong.html
function distanceLatLon(latitude1_R, longitude1_R, latitude2_R, longitude2_R) {
    let R = Phys_cnsts.wgs84_radius_earth_km * 1000;

    let φ1 = latitude1_R;
    let λ1 = longitude1_R;
    let φ2 = latitude2_R;
    let λ2 = longitude2_R;

    let Δφ = φ2-φ1;
    let Δλ = λ2-λ1;

    let a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    let distanceToTarget = R * c;

    return distanceToTarget;
}

// Calculate the cross-track distance from a target point to a great circle path
// C.f. https://www.movable-type.co.uk/scripts/latlong.html
function crossTrackDistance(trackAngle_R, bearing_R, distanceToTarget) {
    let R = Phys_cnsts.wgs84_radius_earth_km * 1000;

    let θ12 = trackAngle_R;
    let θ13 = bearing_R;
    let δ13 = distanceToTarget / R;
    let dXt = Math.asin(Math.sin(δ13)*Math.sin(θ13-θ12)) * R;

    return dXt;

}

// Calculate the along-track distance from a target point to the closest point on a great circle path
// C.f. https://www.movable-type.co.uk/scripts/latlong.html
function alongTrackDistance(xTrackDistance, distanceToTarget) {
    let R = Phys_cnsts.wgs84_radius_earth_km * 1000;

    let δ13 = distanceToTarget / R;
    let dXt = xTrackDistance;
    let dAt = Math.acos(Math.cos(δ13)/Math.cos(dXt/R)) * R;

    return dAt;

}
