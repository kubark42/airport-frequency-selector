.pragma library

//TODO: Declare everything here as constants

var SYS_METRIC = 0
var SYS_IMPERIAL = 1
var SYS_FATHOMS = 2

// Distance conversions
var meters_to_feet = 1/(0.0254*12)
var meters_to_fathoms = meters_to_feet*feet_to_fathoms
var feet_to_fathoms = 1/6.0
var feet_to_miles = 1/5280.0
var nm_to_deg_lat = 60.0  // 60 nautical miles per degree latitude
var deg_lat_to_nm = 1.0/nm_to_deg_lat  // 1 degree latitude per 60 nautical miles
var km_to_nm = 1.609*1.15
var nm_to_km = 1/km_to_nm

// Speed conversions
var mps_to_kph = 3.6 // Meters per second to kilometers per hour
var kph_to_mps = (1/mps_to_kph) // Kilometers per hour to meters per second
var mps_to_kts = mps_to_kph/km_to_nm // Convert meters per second to knots
var kts_to_mps = 1/mps_to_kts // Convert knots to meters per second
var fps_to_mps = 0.3048 // Feet per second to meters per second

// Trigonometry values
var deg2rad = Math.PI / 180
var rad2deg = 180 / Math.PI

// WGS84 coefficients
var wgs84_radius_earth_km =         6371.008 // Earth's radius, in [km]
var wgs84_a =                       6378.137 // semi-major axis of the ellipsoid, in [km]
var wgs84_b =                   6356.7523142 // semi-minor axis of the ellipsoid, in [km]
var wgs84_flattening =   3.35281066474748e-3 // flattening, i.e. (1 / 298.257223563)
var wgs84_eps =           8.1819190842622e-2 // first eccentricity, i.e. sqrtf(1-WGS84_B^2/WGS84_A^2)
var wgs84_eps2 =              6.694379990e-3 // first eccentricity squared, i.e. WGS84_EPS^2

