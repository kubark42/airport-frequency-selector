# Airport Frequency Selector

Helps airport managers determine which standard UNICOM frequencies are uncluttered near the airport.

## Releases
**Releases**: https://gitlab.com/kubark42/airport-frequency-selector/-/releases

## Instructions
Usage is intuitive:

- There's a search box in the top right corner where the user types the airport designation.
- The program automatically displays the airpot (blue diamond) and all airports within 200km which share its frequency (magneta circles).
- Other frequencies can be selected from the drop-down box in the top right.
- Requires an internet connection to show the aviation sectional.

The list of airport frequencies was generated using https://gitlab.com/kubark42/aixm-data-extraction. It is up-to-date as of [the all_airports_freq.json file](https://gitlab.com/kubark42/airport-frequency-selector/-/blob/main/Resources/all_airports_freq.json).

## Screenshots

For IOB (Mt. Sterling, KY) We can see that 122.8 is a crowded frequency:
![a](https://gitlab.com/kubark42/airport-frequency-selector/uploads/fec9f474c7bdef6b83b65121ca55684b/Screen_Shot_2021-09-14_at_17.14.30.png)

122.975 is much better:
![b](https://gitlab.com/kubark42/airport-frequency-selector/uploads/6def3f042fd2df342434e2fb1a3e37d3/Screen_Shot_2021-09-14_at_17.15.56.png)

## Building
Almost bog standard Qt build.

1. Download and install Qt5
1. Download and install [ArcGIS SDK](https://developers.arcgis.com/)
1. [Generate an ArcGIS API key](https://developers.arcgis.com/api-keys) and copy it to the `const QString apiKey = QStringLiteral("");` in https://gitlab.com/kubark42/airport-frequency-selector/-/blob/main/main.cpp
1. Load `AirportFrequencySelector.pro` into Qt Creator and hit the big build button. This should always work, if the build fails, let me know and I'll fix it.
