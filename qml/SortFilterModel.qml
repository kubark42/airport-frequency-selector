import QtQuick 2.15
import QtQml.Models 2.15

DelegateModel {
    id: delegateModel

    property var lessThan: function(left, right) { return true; }
    property var filterAcceptsItem: function(item) { return true; }

    function update() {
        if (items.count > 0) {
            items.setGroups(0, items.count, "items");
        }

        // Step 1: Filter items
        let visible = [];
        let item;
        for (let i = 0; i < items.count; ++i) {
            item = items.get(i);
            if (filterAcceptsItem(item.model)) {
                visible.push(item);
            }
        }

        // Step 3: Add all items to the visible group:
        for (let i = 0; i < visible.length; ++i) {
            item = visible[i];
            item.inVisible = true;
            if (item.visibleIndex !== i) {
                visibleItems.move(item.visibleIndex, i, 1);
            }
        }

        // Emit completed signal
        filteringCompleted()
    }

    property bool isModelInitiated: false

    onIsModelInitiatedChanged: isModelInitiated ? update() : undefined

    items.onChanged: isModelInitiated ? update() :  undefined
    onLessThanChanged: isModelInitiated ? update() :  undefined
    onFilterAcceptsItemChanged: isModelInitiated ? update() :  undefined

    property int visibleCount: visibleItems.count
    signal filteringCompleted

    function get(idx) {
        return visibleItems.get(idx).model
    }

    groups: DelegateModelGroup {
        id: visibleItems

        name: "visible"
        includeByDefault: false
    }

    filterOnGroup: "visible"
}
