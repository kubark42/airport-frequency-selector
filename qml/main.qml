import QtQuick 2.15
import QtQuick.Controls 2.15
import Esri.ArcGISRuntime 100.11

import Qt.labs.settings 1.0


import QtQml.Models 2.15
import QtQuick.Layouts 1.15

import QtPositioning 5.15

import "../js/physical_constants.js" as Phys_cnsts
import "../js/helper.js" as Helper

ApplicationWindow {
    id: mainWindow
    width: 800
    height: 600
    title: "AirportFrequencySelector"

    property string searchString
    property int airportIdx
    property real unicomFrequency
    property var airportCoords


    Rectangle {

        border.width: 1

        anchors.right: parent.right
        width: 100
        height: 20
        z:2
        y: 10

        TextInput {
            id: textInput
            anchors {
                left: parent.left
                right: airportFrequencyText.left
                top: parent.top
                bottom: parent.bottom
            }

            anchors.margins: 5

            onTextChanged: {
                if (text.length === 3 || text.length === 4) {
                    searchString = text
                }
            }
        }

        Text {
            id: airportFrequencyText
            text: "(" + unicomFrequency + ")"

            anchors {
//                left: parent.left
                right: parent.right
                top: parent.top
//                bottom: parent.bottom
            }
        }

        ComboBox {
            id: frequencyComboBox
            anchors.top: textInput.bottom
            anchors.margins: 5

            textRole: "text"
            valueRole: "value"

            model: [{value: 122.7, text: "122.70"},
                {value: 122.725, text: "122.725"},
                {value: 122.8, text: "122.8"},
                {value: 122.9, text: "122.9"},
                {value: 122.95, text: "122.95"},
                {value: 122.975, text: "122.975"},
                {value: 123.0, text: "123.0"},
                {value: 123.05, text: "123.05"},
                {value: 123.075, text: "123.075"}
            ]



            onActivated: {
                unicomFrequency = currentValue*1

                console.log("unicomFrequency: " + unicomFrequency)
                delegateModel.update()
            }
        }
    }

    // add a mapView component
    MapView {
        anchors.fill: parent
        // set focus to enable keyboard navigation
        focus: true

        // add a map to the mapview
        Map {

            // add the ArcGISTopographic basemap to the map
//            initBasemapStyle: Enums.BasemapStyleNone
            initBasemapStyle: Enums.BasemapStyleArcGISNavigation

            // Layer containing all the NOAA weather buoys
            KmlLayer {
                // This is a KML resource that references other KML resources over a network.
                dataset:  KmlDataset {
                    url: "http://www.chartbundle.com/charts/kml/sec.kml"
                }
            }

            // create initial viewpoint
            ViewpointCenter {
                targetScale: 50000000

                Point {
                    x: -12000000
                    y: 5000000
                    spatialReference: SpatialReference { wkid: 3857 }
                }
            }
        }

        // create graphic overlay
        GraphicsOverlay {
            id: graphicsOverlay


            // set renderer for overlay
            SimpleRenderer {

                // set symbol as red cross
                SimpleMarkerSymbol {
                    id: crossSymbol
                    style: Enums.SimpleMarkerSymbolStyleCircle
                    color: "magenta"
                    size: 12
                }
            }

            // add the points to be rendered
            Graphic {

                // Old Faithful
                Point {
                    x: -110.828140
                    y: 44.460458

                    spatialReference: Factory.SpatialReference.createWgs84()
                }
            }
        }
    }

    Settings {
        id: lastApplicationState
        category: "mainLastApplicationState"

        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property alias width: mainWindow.width
        property alias height: mainWindow.height
    }


    ListModel {
        id: frequencyListModel

        Component.onCompleted: {
            var xhr = new XMLHttpRequest;
            xhr.open("GET", "qrc:///Resources/all_airports_freq.json");
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    var data = JSON.parse(xhr.responseText);


                    frequencyListModel.clear();


                    for (var i = 0; i < data.length; i++) {
                        frequencyListModel.append(
                                    {
                                        airport_id: data[i].airport_id,
                                        lat_D: data[i].lat_D,
                                        lon_D: data[i].lon_D,
                                        altitude: data[i].altitude,
                                        sectional: data[i].sectional,
                                        unicom: data[i].unicom,
                                        frequency: data[i].frequency
                                    })
                    }

                    delegateModel.isModelInitiated = true
                }
            }
            xhr.send();
        }
    }


    onSearchStringChanged: {


        airportIdx = -1;
        unicomFrequency = -1
        console.log("searchString: " + searchString)

        // Iterate over the list model`
        for (let i=0; i<frequencyListModel.count; i++) {

            // Check if the airport which matches the search string
            if (frequencyListModel.get(i).airport_id.toLowerCase() === searchString.toLowerCase()) {
                airportIdx = i;

                // Check if there is a unicom frequency, and if not, fall back
                if (frequencyListModel.get(i).unicom !== -1) {
                    unicomFrequency = frequencyListModel.get(i).unicom
                } else if (frequencyListModel.get(i).frequency !== -1){
                    unicomFrequency = frequencyListModel.get(i).frequency
                } else {
                    unicomFrequency = -1
                }

                airportCoords = QtPositioning.coordinate(frequencyListModel.get(i).lat_D, frequencyListModel.get(i).lon_D)

                frequencyComboBox.currentIndex = frequencyComboBox.find(unicomFrequency, Qt.MatchStartsWith)

                delegateModel.update()

                return;
            }
        }

        delegateModel.update()
    }


    function refreshMarkers() {
        graphicsOverlay.graphics.clear()
        // add text symbols

        const simpleMarkerSymbol2 = ArcGISRuntimeEnvironment.createObject("SimpleMarkerSymbol", {
                                                                              style: Enums.SimpleMarkerSymbolStyleDiamond,
                                                                              color: "blue",
                                                                              size: 18
                                                                          });

        graphicsOverlay.graphics.append(createGraphic(createMultipoint(), crossSymbol));
        graphicsOverlay.graphics.append(createGraphic(createMapPoint(), simpleMarkerSymbol2));
    }


    function createMapPoint() {
        // Return a map point for the base airport
        return ArcGISRuntimeEnvironment.createObject("Point", {
                                                         x: airportCoords.longitude,
                                                         y: airportCoords.latitude,
                                                         spatialReference: Factory.SpatialReference.createWgs84()
                                                     });
    }

    function createMultipoint() {
        // Create a polygon builder
        const multipointBuilder = ArcGISRuntimeEnvironment.createObject("MultipointBuilder", {
                                                                            spatialReference: Factory.SpatialReference.createWgs84()
                                                                        });

        for (let i = 0; i < delegateModel.visibleCount; i++) {


            let fred = delegateModel.get(i)

            multipointBuilder.points.addPointXY(fred.lon_D, fred.lat_D);
        }

        // Return the geometry.
        return multipointBuilder.geometry;
    }

    function createGraphic(geometry, symbol) {
        const graphic = ArcGISRuntimeEnvironment.createObject("Graphic");
        graphic.geometry = geometry;
        graphic.symbol = symbol;
        return graphic;
    }

    SortFilterModel {
        id: delegateModel

        lessThan: function(left, right) {
            if (view.sortLexically) {
//                return true
            } else {
                return true; // left.modelData < right.modelData;
            }
        }

        filterAcceptsItem: function(item) {
            // Don't accept the self airport
            if (item.airport_id.toLowerCase() === searchString.toLowerCase()) {
                return false;
            }

            // Do the easy pruning first. If the frequency is not the same, then we can ignore it
            if (item.unicom !== unicomFrequency && item.frequency !== unicomFrequency) {
                return false;
            }

            // Calculate distance. We only care about radio stations which are within 200km
            let dist = Helper.distanceLatLon(
                        airportCoords.latitude * Phys_cnsts.deg2rad, airportCoords.longitude * Phys_cnsts.deg2rad,
                        item.lat_D * Phys_cnsts.deg2rad, item.lon_D * Phys_cnsts.deg2rad)

            return (dist < 200e3)
        }

        model: frequencyListModel
        delegate: Text {
            id: item

            text: airport_id + ": " + ((unicom !== -1) ? unicom : frequency)
        }

        onFilteringCompleted: {
            refreshMarkers()
        }

    }

    ListView {
        id: view

        property bool sortLexically: false
//        visible: false

        anchors {
            left: parent.left
//            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }

        anchors.margins: 5

        width: 90

        model: delegateModel
        onSortLexicallyChanged: delegateModel.update()

        // Place white rectangle behind ListView
        Rectangle {
            z:-1
            color: "white"
            anchors.fill: parent
            anchors.margins: -5
        }
    }

    RowLayout {
        id: controls

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

//        Button {
//            text: qsTr("Toggle odd/even")
//            onClicked: {
//                graphicsOverlay.graphics.clear()
//            }
//        }

//        Button {
//            text: qsTr("Toggle sort")
//            onClicked: view.sortLexically = !view.sortLexically
//        }

//        Slider {
//            id: numberOfItems
//            from: 10
//            to: 1000
//            value: 100
//            Layout.fillWidth: true
//        }
    }
}
